ghps is a tool for displaying a map of unhiked peaks on the
[Hundred Peaks Section](http://www.hundredpeaks.org/)'s peak list.

![Screenshot](screenshot.png)

# Feedback

If you have any feedback about the project, please file an issue at
https://gitlab.com/kraai/ghps or email kraai@ftbfs.org.

# Installation

ghps requires the champlain-gtk and sqlite3 libraries.

On [Debian](https://www.debian.org/), the development libraries can be
installed by running

```shell
apt-get install -y libchamplain-gtk-0.12-dev libsqlite3-dev
```

If you have instructions for other operating systems, please file a
merge request or otherwise let me know.

Once these libraries are installed, run

```shell
make
```

to build ghps.

# Getting started

Install [hps](https://gitlab.com/kraai/hps) and run

```shell
hps init
```

Then run

```shell
ghps
```
