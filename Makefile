all: ghps

clean:
	rm -f ghps

ghps: main.c
	gcc -o $@ $< `pkg-config --cflags --libs champlain-gtk-0.12 sqlite3` -Wall -Wextra -Werror -O2

.PHONY: all clean
