/* Copyright 2017 Matt Kraai

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
   License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <champlain-gtk/champlain-gtk.h>
#include <clutter-gtk/clutter-gtk.h>
#include <sqlite3.h>

static void
go_to_page (G_GNUC_UNUSED ChamplainMarker *self, G_GNUC_UNUSED ClutterEvent *event, gpointer user_data)
{
  gchar *uri = g_strdup_printf ("http://www.hundredpeaks.org/guides/%s.htm", (gchar *)user_data);

#if GTK_CHECK_VERSION(3, 22, 0)
  gtk_show_uri_on_window (NULL, uri, GDK_CURRENT_TIME, NULL);
#else
  gtk_show_uri (NULL, uri, GDK_CURRENT_TIME, NULL);
#endif
}

static int
add_marker (void *data, int number_of_columns, char **column_values, char **column_names)
{
  gdouble latitude;
  gdouble longitude;
  ClutterActor *label;

  g_return_val_if_fail(number_of_columns == 4, 0);
  g_return_val_if_fail(!g_strcmp0(column_names[0], "id"), 0);
  g_return_val_if_fail(!g_strcmp0(column_names[1], "latitude"), 0);
  g_return_val_if_fail(!g_strcmp0(column_names[2], "longitude"), 0);
  g_return_val_if_fail(!g_strcmp0(column_names[3], "name"), 0);

  latitude = g_ascii_strtod (column_values[1], NULL);
  longitude = g_ascii_strtod (column_values[2], NULL);

  label = champlain_label_new_with_text (column_values[3], NULL, NULL, NULL);
  g_signal_connect (G_OBJECT (label), "button-release", G_CALLBACK (go_to_page), g_ascii_strdown(column_values[0], -1));
  champlain_location_set_location (CHAMPLAIN_LOCATION (label), latitude, longitude);
  champlain_marker_layer_add_marker (CHAMPLAIN_MARKER_LAYER (data), CHAMPLAIN_MARKER (label));
  return 0;
}

static int
add_markers (ChamplainMarkerLayer *layer)
{
  gchar *path;
  sqlite3 *db;
  const char *query;
  char *error_message;

  path = g_strconcat (g_get_home_dir (), "/.hps", NULL);

  if (sqlite3_open (path, &db))
    {
      fprintf (stderr, "Can't open database: %s\n", sqlite3_errmsg (db));
      sqlite3_close (db);
      return 1;
    }

  query =
    "select id, latitude, longitude, name from peaks where not suspended and not exists (select * from hikes where peak = peaks.id)";

  if (sqlite3_exec (db, query, add_marker, layer, &error_message))
    {
      fprintf (stderr, "SQL error: %s\n", error_message);
      sqlite3_free (error_message);
      sqlite3_close (db);
      return 1;
    }

  sqlite3_close (db);
  return 0;
}

static void
zoom_out (GtkButton *button G_GNUC_UNUSED, gpointer user_data)
{
  ChamplainView *view;

  view = CHAMPLAIN_VIEW (user_data);
  champlain_view_zoom_out (view);
}

int
main (int argc, char **argv)
{
  GtkWidget *window, *box, *widget, *button;
  ChamplainView *view;
  ChamplainMarkerLayer *layer;

  if (gtk_clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
    return 1;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (gtk_main_quit), NULL);

  box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add (GTK_CONTAINER (window), box);

  widget = gtk_champlain_embed_new ();
  gtk_widget_set_size_request (widget, 640, 480);

  gtk_box_pack_start (GTK_BOX (box), widget, TRUE, TRUE, 0);

  view = gtk_champlain_embed_get_view (GTK_CHAMPLAIN_EMBED (widget));
  champlain_view_center_on (view, 33.711629, -117.755386);
  champlain_view_set_zoom_level (view, 7);

  layer = champlain_marker_layer_new ();
  champlain_view_add_layer (view, CHAMPLAIN_LAYER (layer));

  if (add_markers (layer))
    return 1;

  button = gtk_button_new_with_label ("Zoom out");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (zoom_out), view);
  gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}
